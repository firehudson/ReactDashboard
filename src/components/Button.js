import React from 'react';
import FlexBox from './FlexBox';
import { AuthBoardMainColor, AppBGColor } from '../constants/styles'

const styles = {
  container: {
    backgroundColor: AuthBoardMainColor,
    borderRadius: 3,
    maxHeight: 55,
  },
  label: {
    color: AppBGColor,
    fontSize: 17,
    fontWeight: '600',
    textAlign: 'center',
  },
};

const Button = (props) => {
  const containerStyles = {
    ...styles.container,
    ...props.containerStyles,
  };

  const labelStyles = {
    ...styles.label,
    ...props.labelStyles,
  };

  return (
    <FlexBox style={containerStyles} {...props}>
      <p style={labelStyles}>{props.label}</p>
    </FlexBox>
  );
};

Button.propTypes = {

};

Button.defaultProps = {

};

export default Button;
