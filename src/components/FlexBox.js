import React from 'react';

const styles = {
  felxbox: {
    display: 'flex',
  },
};

const FlexBox = ({ style, children, flex=1, row, ...props }) => {
  const containerStyles = {
    ...styles.felxbox,
    flex,
    flexDirection: row ? 'row' : 'column',
    ...style,
  };

  return (
    <div style={containerStyles} {...props}>
      {children}
    </div>
  );
}

export default FlexBox;
