import React from 'react';
import { Link } from 'react-router-dom';
import FlexBox from './FlexBox';

import Background from '../assets/not_found.png';

const styles = {
  container: {

  },
  imageSection: {
    backgroundImage: `url(${Background})`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'contain',
    backgroundPositionX: 'center',
  },
};

const NotFound = () => (
  <FlexBox style={styles.container}>
    <FlexBox style={styles.imageSection}>
      <FlexBox />
      <FlexBox>
        <FlexBox>
          <FlexBox />
          <FlexBox style={{  alignItems: 'center', justifyContent: 'center'  }}>
            <Link to={'/auth'}> Get Started </Link>	
          </FlexBox>
        </FlexBox>
        <FlexBox />
      </FlexBox>

    </FlexBox>
  </FlexBox>
);


export default NotFound;
