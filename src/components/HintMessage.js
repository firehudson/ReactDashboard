import React from 'react';
import FlexBox from './FlexBox';
import { InfoTextColor, ErrorTextColor } from '../constants/styles';

const styles = {
  container: {
    maxHeight: 15,
    justifyContent: 'center',
    alignItems: 'flex-end',
    paddingRight: 5,
  },
  hintMessage: {
    fontSize: 12,
  }
};

const HintMessage = ({ error, info }) => {
  const textStyles = {
    ...styles.hintMessage,
    color: error ? ErrorTextColor : InfoTextColor,
  };

  return (
    <FlexBox style={styles.container}>
      {
        error || info && <p style={textStyles}>
          {error || info}
        </p>
      }
    </FlexBox>
  );
};

HintMessage.propTypes = {

};

HintMessage.defaultProps = {

};

export default HintMessage;
