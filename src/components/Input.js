import React from 'react';
import _ from 'lodash';
import FlexBox from './FlexBox';
import {
  LightFontColor,
  LightBorderColor,
  ErrorTextColor,
} from '../constants/styles'

const styles = {
  container: {
    margin: 2,
    borderRadius: 3,
    borderWidth: 1,
    borderColor: LightBorderColor,
    borderStyle: 'solid',
    paddingLeft: 10,
    paddingRight: 10,
  },
  input: {
    flex: 1,
    margin: 2,
    borderWidth: 0,
    outline: 'none',
    fontSize: 16,
  }
};

const Input = props => {
  const containerStyles = {
    ...styles.container,
    borderColor: props.hasError ? ErrorTextColor : LightBorderColor,
  };

  const inputProps = _.omit(props, ['leftElement', 'rightElement', 'hasError', 'assignRef', 'style', 'containerStyle', 'inputStyle']);

  return (
    <FlexBox row style={{...containerStyles, ...props.containerStyle}}>
      { props.leftElement && props.leftElement }

      <input
        style={{...styles.input, ...props.inputStyle}}
        {...inputProps}
        ref={props.assignRef}
      />

      { props.rightElement && props.rightElement }
    </FlexBox>
  );
}

Input.propTypes = {

};

Input.defaultProps = {

};

export default Input;
