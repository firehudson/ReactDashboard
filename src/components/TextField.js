import React from 'react';
import FlexBox from './FlexBox';
import Input from './Input';
import HintMessage from './HintMessage';

const styles = {
  container: {
    // backgroundColor: 'grey',
    maxHeight: 70,
  },
  hintWrapper: {
    maxHeight: 15,
    justifyContent: 'center',
    alignItems: 'flex-end',
    paddingRight: 5,
  },
  hintMessage: {
    fontSize: 10,
  }
};

const TextField = props => (
  <FlexBox style={styles.container}>
    <Input hasError={props.hint} {...props} />
    <HintMessage {...props.hint} />
  </FlexBox>
);

TextField.propTypes = {
};

TextField.defaultProps = {

};

export default TextField;
