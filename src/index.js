import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom'
import registerServiceWorker from './registerServiceWorker';
import App from './containers/App';
import store from './store';
import Routing from './routes';
import './index.css';

ReactDOM.render(
  <BrowserRouter>
    <Provider store={store} >
      <App>
        <Routing />
      </App>
    </Provider>
  </BrowserRouter>,
  document.getElementById('root')
);

registerServiceWorker();
