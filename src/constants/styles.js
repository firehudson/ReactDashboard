// background colors
export const AppBGColor = 'white';
export const AuthBoardMainColor = 'rgb(90,200,235)';
// export const AppBGColor = 'linear-gradient(to bottom right, rgb(180,160,160), rgb(130,120,120))';

export const LightBorderColor = 'rgb(200, 200, 200)';
export const InfoTextColor = '#31708f';
export const ErrorTextColor = '#a94442';

// fonts
export const AppFontFamily = 'Monaco, monospace';
export const LightFontColor = '#808080';
// export const AppFontFamily = 'sans-serif';