import _ from 'lodash';
import * as actions from '../constants/actionTypes';

const initialState = {};

const formData = (state=initialState, action) => {
  if(!action.type)
    return state;

  switch (action.type) {
    case actions.UPDATE_FORM_DATA:
      return _.set(state, action.path, action.value);

    default:
      return state;
  }
}

export default formData;
