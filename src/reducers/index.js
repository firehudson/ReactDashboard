import { combineReducers } from 'redux';
import formData from './FormData';

const reducers = {
  formData,
};

export default combineReducers(reducers);
