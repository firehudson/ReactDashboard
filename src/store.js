import { createStore, applyMiddleware, compose } from 'redux';
import reducers from './reducers';
import Logger from 'redux-logger';

export const configureStore = (initialState = {}, logger) => {
  const middleWares = [];

  if (logger)
    middleWares.push(Logger);

  const enhancer = compose(applyMiddleware(...middleWares));

  const Store = createStore(reducers, initialState, enhancer);

  if (module.hot) {
    module.hot.accept(() => {
      const nextRootReducer = require('./reducers').default;
      Store.replaceReducer(nextRootReducer);
    });
  }

  return Store;
}

const store = configureStore({}, true);

export default store;
