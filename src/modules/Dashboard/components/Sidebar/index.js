import React from 'react';

const styles = {
  container: {
		float: 'left',
		width: 276,
		flexDirection: 'column',
		backgroundColor: 'grey',
	},
  listItem: {
    backgroundColor: 'rgb(255, 255, 255)',
    borderBottom: '0.5px solid rgb(244, 244, 244)',
    height: 54,
    width: '100%',
  },
  itemIcon: {
    width: 54,
    height: 54,
    justifyContent: 'center',
    alignItems: 'center',
    borderRight: '0.5px solid rgb(244, 244, 244)',
  },
  itemIconSelected: {
    width: 54,
    height: 54,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'grey'
  },
  itemTitle: {
    paddingLeft: 20,
    justifyContent: 'center',
    flexDirection: 'column',
    flex: 1,
  },
  itemTitleSelected: {
    flex: 1,
    paddingLeft: 20,
    justifyContent: 'center',
    backgroundColor: 'green',
    flexDirection: 'column',
  },
};

const Sidebar = () => <div style={styles.container}>
  <div style={styles.listItem}>
    <div style={styles.itemIconSelected}>X</div>
    <div style={styles.itemTitleSelected}>BOOKS</div>
  </div>
  <div style={styles.listItem}>
    <div style={styles.itemIcon}>X</div>
    <div style={styles.itemTitle}>STUDENTS</div>
  </div>
  <div style={styles.listItem}>
    <div style={styles.itemIcon}>X</div>
    <div style={styles.itemTitle}>AUTHORS</div>
  </div>
  <div style={styles.listItem}>
    <div style={styles.itemIcon}>X</div>
    <div style={styles.itemTitle}>PUBLICATINOS</div>
  </div>
  <div style={styles.listItem}>
    <div style={styles.itemIcon}>X</div>
    <div style={styles.itemTitle}>ACTIVITIES</div>
  </div>
</div>;

export default Sidebar;
