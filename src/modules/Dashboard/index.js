import React from 'react';
import { Route, Switch } from 'react-router-dom';

import DashboardContent from './containers/Dashboard';
import Sidebar from './components/Sidebar';
import Navbar from './components/Navbar';

const styles = {
	container: {
		flex: 1,
		opacity: 0.8,
	},
	dashboard: {
		flex: 1,
		flexDirection: 'column',
		backgroundColor: 'white',
		margin: '30px 60px 30px 60px',
		borderRadius: 4,
		boxShadow: '0 15px 20px 8px rgba(0, 0, 0, .3), 0 1px 4px rgba(0, 0, 0, 0.8), 0 0 40px rgba(0, 0, 0, 0.1) inset',
		overflow: 'hidden',
	},
	contentContainer: {
		flex: 1,
		flexDirection: 'row',
		backgroundColor: 'red',
	},
	dock: {
		flex: 1,
		flexDirection: 'column',
		backgroundColor: 'green',
	},
};

const TestComponent = () => <div><h1>Test Component 1</h1></div>
const TestComponent2 = ({ match }) => <div><h1>Test Component 2</h1></div>
const TestComponent3 = ({ match }) => <div><h1>Test Component 3</h1></div>

const Dashboard = ({ match }) => (
	<div style={styles.container}>
		<div style={styles.dashboard}>
			<Navbar />
			<div style={styles.contentContainer}>
				<Sidebar />
				<DashboardContent>
					<Switch>
						<Route exact path={match.path} component={TestComponent} />
						<Route path={`${match.path}/test`} component={TestComponent2} />
						<Route path={`${match.path}/test1`} component={TestComponent3} />
					</Switch>
				</DashboardContent>
			</div>
		</div>
	</div>
);

export default Dashboard;