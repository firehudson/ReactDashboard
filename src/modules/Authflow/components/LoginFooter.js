import React from 'react';
import { Link } from 'react-router-dom';
import FlexBox from '../../../components/FlexBox';
import { LightFontColor } from '../../../constants/styles';

const styles = {
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 12,
  },
  ligntText: {
    color: LightFontColor,
  },
  signupLink: {
    textDecoration: 'none',
    color: 'black',
  },
};

const LoginFooter = () => (
  <FlexBox style={styles.container} row>
    <p style={styles.ligntText}> Don't have an account yet? </p>
    &nbsp;
    <Link style={styles.signupLink} to={`/user/signup`}><b> Signup Now </b></Link>
  </FlexBox>
);

export default LoginFooter;
