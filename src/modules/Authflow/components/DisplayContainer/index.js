import React from 'react';
import FlexBox from '../../../../components/FlexBox';
import { AuthBoardMainColor } from '../../../../constants/styles';

const styles = {
  container: {
    backgroundColor: AuthBoardMainColor,
  },
};

const DisplayContainer = () => (
  <FlexBox style={styles.container} />
);

export default DisplayContainer;
