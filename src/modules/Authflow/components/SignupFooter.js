import React from 'react';
import { Link } from 'react-router-dom';
import FlexBox from '../../../components/FlexBox';
import { LightFontColor } from '../../../constants/styles';

const styles = {
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 12,
  },
  ligntText: {
    color: LightFontColor,
  },
  signupLink: {
    textDecoration: 'none',
    color: 'black',
  },
};

const SignupFooter = () => (
  <FlexBox style={styles.container} row>
    <p style={styles.ligntText}> Already have an account? </p>
    &nbsp;
    <Link style={styles.signupLink} to={`/user/login`}><b> Login Here </b></Link>
  </FlexBox>
);

export default SignupFooter;
