import React from 'react';
import FlexBox from '../../../components/FlexBox';
import { LightFontColor } from '../../../constants/styles';
import { getGreetingsPrefixMessage } from '../../../utils/greetings';

const styles = {
  container: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  ligntText: {
    color: LightFontColor,
    fontSize: 12,
  },
  heroText: {
    color: 'black',
    fontSize: 17,
    fontWeight: '600',
  },
};

const LoginFooter = () => (
  <FlexBox style={styles.container}>
    <b style={styles.heroText}> {getGreetingsPrefixMessage()} Welcome Back</b>
    <p style={styles.ligntText}> Enter your details below here </p>
  </FlexBox>
);

export default LoginFooter;
