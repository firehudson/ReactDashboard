import React from 'react';
import FlexBox from '../../../../components/FlexBox';
import FormHeader from './FormHeader';

const FormContainer = ({ children }) => (
  <FlexBox>
    <FormHeader />
    <FlexBox flex={7}>
      {children}
    </FlexBox>
  </FlexBox>
);

export default FormContainer;
