import React from 'react';
import FlexBox from '../../../../components/FlexBox';

const styles = {
  logo: {
    margin: 10,
    backgroundImage: 'url(https://s-media-cache-ak0.pinimg.com/originals/0a/cd/50/0acd5002683fbcf2b720004f201ee530.jpg)',
    backgroundSize: 'cover',
  },
}

const FormHeader = ({ children }) => (
  <FlexBox flex={1} row>
    <FlexBox style={styles.logo} />
    <FlexBox flex={2} />
  </FlexBox>
);

export default FormHeader;
