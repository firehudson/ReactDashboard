import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import AuthBoard from './containers/AuthBoard';
import Login from './containers/Login';
import Signup from './containers/Signup';

const redirectToLogin = () => <Redirect to={{ pathname: '/user/login' }} />

const Authflow = ({ match }) => (
  <Switch>
    <AuthBoard>
      <Route exact path={`${match.path}`} render={redirectToLogin} />
      <Route path={`${match.path}/login`} component={Login} />      
      <Route path={`${match.path}/signup`} component={Signup} />
    </AuthBoard>
  </Switch>
);

export default Authflow;