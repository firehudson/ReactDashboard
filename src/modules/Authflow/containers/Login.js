import React from 'react';
import FlexBox from '../../../components/FlexBox';
import LoginFooter from '../components/LoginFooter';
import LoginGreetings from '../components/LoginGreetings';
import TextField from '../../../components/TextField';
import Button from '../../../components/Button';
import connectForm from '../../../utils/connectForm';

const styles = {
  fieldsWrapper: {
    marginLeft: '20%',
    marginRight: '20%',
  },
  buttonWrapper: {

  },
}

const Login = (props) => (
  <FlexBox>
    <FlexBox flex={1} />
    <LoginGreetings />
    <FlexBox flex={4} style={styles.fieldsWrapper}>
      <FlexBox flex={2}>
        <TextField
          placeholder={'Username'}
          {...props.handleUpdate('username')}
        />
        <TextField
          placeholder={'Password'}
          {...props.handleUpdate('password')}
        />
      </FlexBox>
      <FlexBox style={styles.buttonWrapper}>
        <Button
          label={'LOGIN'}
          onClick={() => alert('TODO: disable button and try submit form!')}
        />
      </FlexBox>
      <FlexBox />
    </FlexBox>
    <LoginFooter />
  </FlexBox>
);

export default connectForm(Login);
