import React, { Component } from 'react';
import FlexBox from '../../../components/FlexBox';
import DisplayContainer from '../components/DisplayContainer';
import FormContainer from '../components/FormContainer';

const styles = {
  container: {
    paddingLeft: '5vw',
    paddingRight: '5vw',
    paddingTop: '10vh',
    paddingBottom: '10vh',
  },
  wrapper: {
    boxShadow: '0 15px 20px 8px rgba(0, 0, 0, .1), 0 1px 4px rgba(0, 0, 0, 0.8), 0 0 40px rgba(0, 0, 0, 0.1)',
  },
}

export default class Authboard extends Component {
  render() {
    return (
      <FlexBox style={styles.container}>
        <FlexBox style={styles.wrapper} row>
          <FormContainer>
            {this.props.children}
          </FormContainer>
          <DisplayContainer />
        </FlexBox>
      </FlexBox>
    );
  }
}
