import React from 'react';
import FlexBox from '../../../components/FlexBox';
import SignupFooter from '../components/SignupFooter';
import SignupGreetings from '../components/SignupGreetings';
import TextField from '../../../components/TextField';
import Button from '../../../components/Button';
import connectForm from '../../../utils/connectForm';

const styles = {
  fieldsWrapper: {
    marginLeft: '20%',
    marginRight: '20%',
  },
}

const Signup = (props) => (
  <FlexBox>
    <FlexBox flex={1} />
    <SignupGreetings />
    <FlexBox flex={4} style={styles.fieldsWrapper}>
      <FlexBox flex={3}>
        <TextField
          placeholder={'Username'}
          {...props.handleUpdate('username')}
        />
        <TextField
          placeholder={'Email'}
          {...props.handleUpdate('email')}
        />
        <TextField
          placeholder={'Password'}
          {...props.handleUpdate('password')}
        />
      </FlexBox>
      <FlexBox>
        <Button
          label={'SIGN UP'}
          onClick={() => alert('TODO: disable button and try submit form!')}
        />
      </FlexBox>
    </FlexBox>
    <SignupFooter />
  </FlexBox>
);

export default connectForm(Signup);
