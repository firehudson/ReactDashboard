import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Dashboard from './modules/Dashboard';
import Authflow from './modules/Authflow';
import NotFound from './components/NotFound';

const Routing = () => (
	<Switch>
		<Route path='/user' component={Authflow} />
		<Route path='/dashboard' component={Dashboard} />
		<Route component={NotFound} />
	</Switch>
);

export default Routing;
