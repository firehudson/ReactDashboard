import React, { Component } from 'react';
import FlexBox from '../components/FlexBox';
import { AppBGColor, AppFontFamily } from '../constants/styles';

const styles = {
  container: {
    margin: 0,
    padding: 0,
    fontFamily: AppFontFamily,
    background: AppBGColor,
    width: '100vw',
    height: '100vh',
  },
};

class App extends Component {
  render() {
    return (
      <FlexBox style={styles.container}>
        {this.props.children}
      </FlexBox>
    );
  }
}

export default App;
