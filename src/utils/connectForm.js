import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';
import { UPDATE_FORM_DATA } from '../constants/actionTypes';
import mergeObjects from './mergeObjects';

const connectForm = (Component, subStoreKey) => {
  const updateFormData = (path, value) => {
    const arrayPath = _.isArray(path) ? path : [path];

    return {
      type: UPDATE_FORM_DATA,
      path: subStoreKey ? [subStoreKey, ...arrayPath] : arrayPath,
      value
    };
  };

  const handleUpdate = (props, path) => ({
    value: _.get(props.formData, path),
    onChange: (e) => props.updateFormData(path, e.target.value),
  });

  const mapStateToProps = ({ formData }) => ({
    formData: subStoreKey ? formData[subStoreKey] : formData,
  });

  const mapDispatchToProps = dispatch => (
    bindActionCreators({ updateFormData }, dispatch)
  );

  const mergedProps = (...params) => {
    const allProps = mergeObjects(params);

    return {
      ...allProps,
      handleUpdate: (path) => handleUpdate(allProps, path)
    };
  }

  return connect(mapStateToProps, mapDispatchToProps, mergedProps)(Component);
}

export default connectForm;
