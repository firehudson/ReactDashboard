export default objects => {
  let targetObject = {};

  objects.forEach(object => {
    targetObject = {
      ...targetObject,
      ...object
    };
  });

  return targetObject;
};
