import moment from 'moment';

export const getGreetingsPrefixMessage = () => {
  const hour = moment().hours();

  if(hour < 12)
    return 'Good Morning!';

  if(hour > 12 && hour <= 18)
    return 'Good Afternoon!';

  if(hour > 18)
    return 'Good Evening!';
}